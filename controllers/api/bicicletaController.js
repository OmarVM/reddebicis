var Bicicleta = require('../../models/bicicleta')

exports.listaDeBicicletas = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.crearBicicleta = function(req, res){
    var ubicacion = [req.body.lat,req.body.lon]
    var bici = new Bicicleta(
        req.body.id,
        req.body.color,
        req.body.modelo,
        ubicacion
    )

    Bicicleta.add(bici)

    res.status(200).json({bici})
}

exports.actualizarBicicleta = function(req, res){
    var bici = Bicicleta.findById(req.body.id);

    bici.id = req.body.id
    bici.color = req.body.color
    bici.modelo = req.body.modelo
    bici.ubicacion = [req.body.lat,req.body.lon]

    res.status(200).json({bici})
}

exports.eliminarBicicleta = function(req, res){
    Bicicleta.removeById(req.body.id)

    res.status(200).json({
        mensaje: `Bicicleta ${req.body.id} eliminada exitosamente`
    })
}