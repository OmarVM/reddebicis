var express = require('express');
var router = express.Router();
var bicicleta = require('../../controllers/api/bicicletaController')

router.get('/obtener', bicicleta.listaDeBicicletas)
router.post('/crear', bicicleta.crearBicicleta)
router.put('/actualizar', bicicleta.actualizarBicicleta)
router.delete('/eliminar', bicicleta.eliminarBicicleta)

module.exports = router;
