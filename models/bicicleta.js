var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + '. Color: '+ this.color;
}

Bicicleta.allBicis = []

//Methods
Bicicleta.add = function(bici){
    Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = function(id){
    var bicicleta = Bicicleta.allBicis.find(x => x.id == id);
    
    if (bicicleta){
        return bicicleta;
    } else {
        throw new Error(`No existe una bicicleta con el ID: ${id}`);
    }
}

Bicicleta.removeById = function(id){
    for (var i = 0; i<Bicicleta.allBicis.length;i++){
        if (Bicicleta.allBicis[i].id == id){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

var a = new Bicicleta(1, "Azul","BMX",[-34.6012424,-58.3861497]);
var b = new Bicicleta(2, "Negro","Montaña",[-34.596932,-58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;